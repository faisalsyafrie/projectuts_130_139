package syafrie.faisal.a3e_uts_130_139

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*
import syafrie.faisal.a3e_uts_130_139.data_item.R

class SignUpActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        var email = edRegUserName.text.toString()
        var password = edRegPassword.text.toString()


            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this,"Successfully Register", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this,"Username/password incorrect ", Toast.LENGTH_SHORT).show()

                }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnRegister.setOnClickListener(this)
    }
}

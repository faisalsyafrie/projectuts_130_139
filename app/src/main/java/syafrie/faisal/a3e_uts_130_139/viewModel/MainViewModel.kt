package syafrie.faisal.a3e_uts_130_139.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import syafrie.faisal.a3e_uts_130_139.data_item.isi_row
import syafrie.faisal.a3e_uts_130_139.viewModel.Network.repo


class MainViewModel : ViewModel() {
    private  val repo = repo()
    fun fetcUserData() : LiveData<MutableList<isi_row>>{
        val mutableData = MutableLiveData<MutableList<isi_row>>()
        repo.getData().observeForever { databar ->
         mutableData.value = databar
        }
        return  mutableData
    }
}
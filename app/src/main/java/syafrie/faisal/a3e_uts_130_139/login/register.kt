package syafrie.faisal.a3e_uts_130_139.login

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

import syafrie.faisal.a3e_uts_130_139.R
import syafrie.faisal.a3e_uts_130_139.data_item.R


class register : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        register.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var mail = email.text.toString()
        var password = txpassword.text.toString()

        if (mail.isEmpty() || password.isEmpty()) {
            email.error = "Tidak boleh kosong!"
            txpassword.error = "Tidak boleh kosong!"
        } else {
            val prDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
            prDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"))
            prDialog.setTitleText("Registering...")
            prDialog.setCancelable(false)
            prDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(mail, password)
                .addOnCompleteListener {
                    prDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
//                    Toast.makeText(this, "Berhasil Registrasi", Toast.LENGTH_SHORT).show()
                    sukses("Registrasi berhasil :)")

                }
                .addOnFailureListener {
                    prDialog.hide()
                    alert("Cek Email & Password!")

                }
        }
    }

    fun alert(string: String?) {
        val allAlert = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        allAlert.setCancelable(false)
        allAlert.setTitleText("Maaf")
            .setContentText(string)
            .show()
    }

    fun sukses(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setTitleText("Selamat")
            .setContentText(string)
            .setConfirmText("Login")
            .setConfirmClickListener { sDialog -> sDialog.dismissWithAnimation()
            finish()
            }
            .show()

    }
}

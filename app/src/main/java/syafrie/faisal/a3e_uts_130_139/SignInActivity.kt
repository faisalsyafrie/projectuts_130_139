package syafrie.faisal.a3e_uts_130_139

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*
import syafrie.faisal.a3e_uts_130_139.data_item.R

class SignInActivity : AppCompatActivity() {

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogoff.setOnClickListener {
            fbAuth.signOut()
            System.exit(0)
        }
    }

}

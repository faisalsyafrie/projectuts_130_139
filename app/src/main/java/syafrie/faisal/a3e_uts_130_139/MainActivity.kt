package syafrie.faisal.a3e_uts_130_139

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import syafrie.faisal.a3e_uts_130_139.data_item.R

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin->{
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()


                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            edUserName.setText("")
                            edPassword.setText("")
                            edUserName.requestFocus()
                            Toast.makeText(this,"Successfully Login",Toast.LENGTH_SHORT).show()
                            val intent = Intent(this,SignInActivity::class.java)
                            startActivity(intent)
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this,"Username/password incorrect",Toast.LENGTH_SHORT).show()

                        }

            }
            R.id.txSignUp->{
                val intent = Intent(this,SignUpActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
        txSignUp.setOnClickListener(this)
    }
}

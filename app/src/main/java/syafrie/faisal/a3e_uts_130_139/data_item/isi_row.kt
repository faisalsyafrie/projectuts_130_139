package syafrie.faisal.a3e_uts_130_139.data_item

data class isi_row(
    val imageUrl: String = "Default URL",
    val nama: String = "Default Name",
    val stock :String = "0",
    val harga: String = "Rp. 0",
    val status : String = "Habis",
    val keterangan: String = "-",
    val id: String = "-"

)
package syafrie.faisal.a3e_uts_130_139.serviceNotifikasi.setingNotif


import syafrie.faisal.a3e_uts_130_139.serviceNotifikasi.setingNotif.Constan.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import dagger.internal.DoubleCheck.lazy as lazy

class RetrofitInstance {
    companion object {
        private val retrovit by lazy {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        val api by lazy {
            retrovit.create(NotifikasiApi::class.java)
        }
    }
}